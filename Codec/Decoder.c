/*!
 * \file Decoder.c
 * \author Андрей Шиганцов
 * \brief
 */

#include "Decoder.h"

void init_NAVTEX_Decoder(NAVTEX_Decoder_t *This, NAVTEX_DecoderCfg_t* cfg)
{
  This->Parent = 0;
  This->on_start = 0;
  This->on_end = 0;
  This->on_char = 0;

  This->endCount = cfg->AlphaCountForEnd;

  This->isSync = 0;
  This->endCounter = 0;

  int i;
  // Очистка буфера временного канала DX
  for (i = 0; i < 3; i++)
    This->DX[i] = NAVTEX_CODE_ERROR;
  // Очистка буфера временного канала RX
  for (i = 0; i < 3; i++)
    This->RX[i] = NAVTEX_CODE_ERROR;
}

// Опознователь начала данных
static inline Bool_t syncIdentifier(Int8_t DX, Int8_t RX)
//! Возвращает флаг готовности приёма данных
{
  return ( (DX == NAVTEX_CODE_RQ)&&(RX == NAVTEX_CODE_ALPHA) );
}

// Опознаватель конца сообщения
static inline Bool_t endIdentifier(Int8_t DX, Int8_t DX0)
{
  return ( (DX == NAVTEX_CODE_ALPHA)&&(DX == DX0) );
}

void navtex_decoder_1(NAVTEX_Decoder_t* This, NAVTEX_Code_t Code)
{
  // Заполнение буферов временных каналов DX и RX
  This->DX[2] = This->DX[1];
  This->RX[2] = This->RX[1];
  This->DX[1] = This->DX[0];
  This->RX[1] = This->RX[0];
  This->DX[0] = Code[0];
  This->RX[0] = Code[1];

  // Определение начала данных
  if ( !This->isSync && syncIdentifier(This->DX[2], This->RX[0]) )
  {
    if (This->on_start)
      This->on_start(This->Parent);
    This->isSync = 1;
    This->endCounter = 0;
  }

  if (This->isSync)
  {
    NAVTEX_DecoderChar_t DecoderChar = {0,navtex_ERROR_NULL};
    // Декодирование текущего символа и определение достоверности
    //------------------------------------------------------------
    // Сравнение данных из каналов DX и RX
    if (This->DX[2] == This->RX[0])
    {
      if (This->DX[2] == NAVTEX_CODE_ALPHA)
      {
        DecoderChar.Data = NAVTEX_SEQUENCE_END;
        DecoderChar.Reliability = navtex_ERROR_NULL;
      }
      else
      {
        DecoderChar.Data = navtex_decode(This->DX[2]);
        if (DecoderChar.Data == NAVTEX_CODE_ERROR)
          DecoderChar.Reliability = navtex_ERROR_2CH;
        else
          DecoderChar.Reliability = navtex_ERROR_NULL;
      }
    }
    else
    {
      // Проверка символа из DX на символ алфавита
      DecoderChar.Data = navtex_decode(This->DX[2]);
      if (DecoderChar.Data != NAVTEX_CODE_ERROR)
      {
        if (navtex_decode(This->RX[0]) == NAVTEX_CODE_ERROR)
          DecoderChar.Reliability = navtex_ERROR_1CH;
        else
          DecoderChar.Reliability = navtex_ERROR_2CH;
      }
      else
      {
        // Проверка символа из RX на символ алфавита
        DecoderChar.Data = navtex_decode(This->RX[0]);
        if (DecoderChar.Data != NAVTEX_CODE_ERROR)
          DecoderChar.Reliability = navtex_ERROR_1CH;
        else
        {
          // Проверка символов из DX и RX на синхропоследовательность
          if ((This->DX[2] == NAVTEX_CODE_RQ)&&(This->RX[0] == NAVTEX_CODE_ALPHA))
          {
            DecoderChar.Reliability = navtex_ERROR_NULL;
            DecoderChar.Data = NAVTEX_SEQUENCE_SYNC;
          }
          else
            DecoderChar.Reliability = navtex_ERROR_2CH;
        }
      }
    }
    //------------------------------------------------------------
    // Декодирование текущего символа и определение достоверности

    if (This->on_char)
      This->on_char(This->Parent, &DecoderChar);

    // Определение окончания сообщения
    if (endIdentifier(This->DX[0], This->DX[1]))
    {
      This->endCounter++;
      if (This->endCounter == This->endCount)
      {
        if (This->on_end)
          This->on_end(This->Parent);
        This->isSync = 0;
        This->endCounter = 0;
      }
    }
  }
}
