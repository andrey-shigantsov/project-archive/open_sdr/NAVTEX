/*!
 * \file Encoder.c
 * \author Андрей Шиганцов
 * \brief
 */

#include "Encoder.h"

void init_NAVTEX_Encoder(NAVTEX_Encoder_t* This)
{
  This->RX[1] = NAVTEX_CODE_ALPHA;
  This->RX[0] = NAVTEX_CODE_ALPHA;
}

void navtex_encoder_1(NAVTEX_Encoder_t* This, Int8_t Char, NAVTEX_Code_t Code)
{
  switch ((UInt8_t)Char)
  {
  default:
    Code[0] = navtex_encode(Char);
    break;

  case NAVTEX_SEQUENCE_SYNC:
    Code[0] = NAVTEX_CODE_RQ;
    break;

  case NAVTEX_SEQUENCE_END:
    Code[0] = NAVTEX_CODE_ALPHA;
    break;
  }

  Code[1] = This->RX[1];
  This->RX[1] = This->RX[0];
  if ( (Code[0] == NAVTEX_CODE_RQ)||(Code[0] == NAVTEX_CODE_ALPHA) )
    This->RX[0] = NAVTEX_CODE_ALPHA;
  else
    This->RX[0] = Code[0];
}
