/*!
 * \file code.c
 * \author Андрей Шиганцов
 * \brief
 */

#include "code.h"

// Возвращает код символа алфавита №2 в передаваемом сигнале
Int8_t navtex_encode(Int8_t ita2Char)
{
  int i;
  i = ita2_index(ita2Char);
  if (i == -1) return NAVTEX_CODE_ERROR;

  return NAVTEX_CodeTabel[i];
}

// Возвращает символа алфавита №2 из кода передаваемого сигнала
Int8_t navtex_decode(Int8_t Code)
{
  int i;
  i = navtex_code_index(Code);
  if (i == NAVTEX_CODE_ERROR) return i;

  return ITA2_Table[i];
}

// Возвращает номер передаваемого кода или -1(ошибка)
Int8_t navtex_code_index(Int8_t Code)
{
  int i;

  for (i = 0; i < ITA2_CHARACTERS_COUNT; i++)
    if (Code == NAVTEX_CodeTabel[i]) return i;

  return NAVTEX_CODE_ERROR;
}

// Передаваемый 7-элементный сигнал
const Int8_t NAVTEX_CodeTabel [ITA2_CHARACTERS_COUNT] =
{
  //	Значение  |	 позиции битов
  //			  |	 1 2 3 4 5 6 7
  //--------------------------------
  0x38,	// BBBYYYB
  0x0D,	// YBYYBBB
  0x62,	// BYBBBYY
  0x2C,	// BBYYBYB
  0x29,	// YBBYBYB
  0x64,	// BBYBBYY
  0x4A,	// BYBYBBY
  0x16,	// BYYBYBB
  0x32,	// BYBBYYB
  0x68,	// BBBYBYY
  0x61,	// YBBBBYY
  0x1A,	// BYBYYBB
  0x46,	// BYYBBBY
  0x26,	// BYYBBYB
  0x0E,	// BYYYBBB
  0x52,	// BYBBYBY
  0x51,	// YBBBYBY
  0x2A,	// BYBYBYB
  0x34,	// BBYBYYB
  0x0B,	// YYBYBBB
  0x31,	// YBBBYYB
  0x43,	// YYBBBBY
  0x58,	// BBBYYBY
  0x45,	// YBYBBBY
  0x54,	// BBYBYBY
  0x1C,	// BBYYYBB
  0x07,	// YYYBBBB
  0x13,	// YYBBYBB
  0x25,	// YBYBBYB
  0x49,	// YBBYBBY
  0x23,	// YYBBBYB
  0x15	// YBYBYBB
};
