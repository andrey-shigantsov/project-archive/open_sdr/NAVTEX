/*!
 * \file ita2.h
 * \author Андрей Шиганцов
 * \brief
 */

#ifndef ITA2_H_
#define ITA2_H_

#include "SDR/BASE/common.h"

#ifdef __cplusplus
extern "C"
{
#endif

#define ITA2_CHARACTERS_COUNT 32

// Символ малтийского креста
#define ITA2_ASCII_MALTIYSKY_CROSS	'#'
// Символ звукового сигнала
#define ITA2_ASCII_SOUND '@'
// Символ буквенного регистра
#define ITA2_ASCII_ALPHABETIC '_'
// Символ цифрового регистра
#define ITA2_ASCII_NUMERIC '^'
// Символ "нет информации"
#define ITA2_ASCII_NO_INF 0
// Зарезервированный символ
#define ITA2_ASCII_RESERVED '&'

// Нет информации
#define ITA2_NO_INF	0x00  	// AAAAA
#define ITA2_ERROR	0xFF

// Индекс начала служебных символов
#define ITA2_SERVICE_CHARS_INDEX 26

// Код международного телеграфного алфавита №2
extern const Int8_t ITA2_Table [ITA2_CHARACTERS_COUNT];
// Соответствующие коды ASCII
extern const Int8_t ITA2_ASCII_Table [ITA2_CHARACTERS_COUNT][2];

// Возвращает символ алфавита №2 из ASCII-кода
Int8_t ita2_encode(Int8_t asciiChar);
// Возвращает символ ASCII-кода из алфавита №2
Int8_t ita2_decode(Int8_t ita2Char, Bool_t isNumeric);
// Возвращает номер символа из алфавита №2 или -1(ошибка)
Int8_t ita2_index(Int8_t ita2Char);
// Возвращает номер символа из алфавита №2 или ITC_ERROR(ошибка) из кода ASCII
Int8_t ita2_ascii_index(Int8_t asciiChar);
// Проверка ASCII-символа на служебный
Bool_t ita2_switch_service(Int8_t asciiChar);
// Проверка буквенного регистра ASCII-символа
Bool_t ita2_switch_alphabetic(Int8_t asciiChar);
// Проверка цифрового регистра ASCII-символа
Bool_t ita2_switch_numeric(Int8_t asciiChar);

#ifdef __cplusplus
}
#endif

#endif /* ITA2_H_ */
